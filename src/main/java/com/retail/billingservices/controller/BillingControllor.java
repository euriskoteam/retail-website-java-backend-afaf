package com.retail.billingservices.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.retail.billingservices.model.Bill;
import com.retail.billingservices.service.BillingService;

@Controller
public class BillingControllor {
	
	private BillingService billingService;
	
	public Double getBillAmount(Bill bill){
		return this.billingService.calculateBillFinalAmount(bill);
	}

	@Autowired
	public void setBillingService(BillingService billingService) {
		this.billingService = billingService;
	}
	
	
	
}
