package com.retail.billingservices.model;

public class BillItem {
	
	private Item item;
	private double quantity;
	
	/**
	 * Bill item constructor given the item values and purchased quantity
	 * @param id
	 * @param description
	 * @param type
	 * @param price
	 * @param quantity
	 */
	public BillItem(String id, String description, ItemType type, double price, double quantity) {
		this.item = new Item(id, description, type, price);
		this.quantity = quantity;
	}
	
	/**
	 * Bill item constructor given the item and purchased quantity
	 * @param item
	 * @param quantity
	 */
	public BillItem(Item item, double quantity) {
		this.item = item;
		this.quantity = quantity;
	}
	
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	
	public ItemType getItemType() {
		return item.getItemType();
	}
	public double getItemPrice() {
		return item.getPrice();
	}
	
	public double getQuantity() {
		return quantity;
	}
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	

}
