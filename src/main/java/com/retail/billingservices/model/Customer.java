package com.retail.billingservices.model;

import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicInteger;

public class Customer {	
	
	private int id;
	private String name;
	private LocalDate createDate;
	private boolean isEmployee;
	private boolean isAffiliate;
	private static final AtomicInteger count = new AtomicInteger(0); 
	
	/**
	 * Customer constructor, generates an id sequence
	 * @param name
	 * @param createDate
	 * @param isEmployee
	 * @param isAffiliate
	 */
	public Customer(String name, LocalDate createDate, boolean isEmployee, boolean isAffiliate){
		this.id = count.incrementAndGet();
		this.name = name;
		this.createDate = createDate;
		this.isEmployee = isEmployee;
		this.isAffiliate = isAffiliate;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDate createDate) {
		this.createDate = createDate;
	}
	public boolean isEmployee() {
		return isEmployee;
	}
	public void setEmployee(boolean isEmployee) {
		this.isEmployee = isEmployee;
	}
	public boolean isAffiliate() {
		return isAffiliate;
	}
	public void setAffiliate(boolean isAffiliate) {
		this.isAffiliate = isAffiliate;
	}
}
