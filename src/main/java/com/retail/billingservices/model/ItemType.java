package com.retail.billingservices.model;

public enum ItemType {
	GROCERY,
    CLEANERS,
    KITCHENWARE,
    PHARMACY,
    PETSUPPLIES,
    ELECTRONICS,
    CLOTHES;
}
