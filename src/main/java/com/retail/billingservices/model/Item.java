package com.retail.billingservices.model;

public class Item {
	
	private String reference;
	private String description;
	private ItemType itemType;
	private double price;

	/**
	 * Item constructor 
	 * @param reference
	 * @param description
	 * @param itemType
	 * @param price
	 */
	public Item (String reference, String description, ItemType itemType, double price){
		this.reference = reference;
		this.description = description;
		this.itemType = itemType;
		this.price = price;
	}
	
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ItemType getItemType() {
		return itemType;
	}
	public void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
}
