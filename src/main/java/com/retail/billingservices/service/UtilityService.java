package com.retail.billingservices.service;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public final class UtilityService {
	
	/**
	 * format a double value to a precision
	 * @param value
	 * @param precision
	 * @return double
	 */
	public static double formatDouble(double value, int precision) {
		
		String format = "###.#";
		for (int i = 1; i < precision; i++){
		    format+="#";
		}
		DecimalFormat decimalFormat = new DecimalFormat(format);
		String formattedNumber = decimalFormat.format(value);
        return Double.parseDouble(formattedNumber);
    }

	/**
	 * Get the years difference between two dates
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	public static long getYearsDifference(LocalDate fromDate, LocalDate toDate) {
		
	    return ChronoUnit.YEARS.between(fromDate, toDate);
    }
}
