package com.retail.billingservices.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.springframework.stereotype.Service;

import com.retail.billingservices.model.Bill;
import com.retail.billingservices.model.BillItem;
import com.retail.billingservices.model.Customer;
import com.retail.billingservices.model.ItemType;

@Service("service")
public class BillingService {
	
	/**
	 * Given a bill customer and purchased items, calculates the final bill amount with discount
	 * @param bill
	 * @return the total bill amount with discount
	 */
	public double calculateBillFinalAmount(Bill bill){

		int discount = this.getCustomerDiscountPercentage(bill.getCustomer());
		double totalAmount;
		
		//Calculate bill amount with discount, apply discount to every 100
		if(discount > 0) {
			totalAmount = this.calculateDiscountedTotalAmount(bill.getItems(), discount);
		} else {
			totalAmount = this.calculateBillTotalAmount(bill.getItems());
		}
		//Fix formatting
		BigDecimal totalAmountFixed = new BigDecimal(totalAmount);
		totalAmount = UtilityService.formatDouble(totalAmountFixed.doubleValue(), 2);

		return totalAmount;
	}
	
	/**
	 * Get the discount percentage of a customer based on his type and join date
	 * @param customer
	 * @return int
	 */
	public int getCustomerDiscountPercentage(Customer customer){
		int discount = 0;
		
		//Check if customer is an employee
		if(customer.isEmployee()){
			discount = 30;
		} else 
			//Check if customer is an affiliate
			if (customer.isAffiliate()) {
			discount = 10;
		} else {
			//Check if customer since 2 years
			LocalDate customerCreateDate = customer.getCreateDate();
			LocalDate currentDate = LocalDate.now();
			long yearsDifference = UtilityService.getYearsDifference(customerCreateDate, currentDate);
			if (yearsDifference >= 2){
				discount = 5;
			}
		}
		
		return discount;
	}
	
	/**
	 * Calculates the bill total amount without taking into consideration any discount
	 * @param items
	 * @return double
	 */
	public double calculateBillTotalAmount( List<BillItem> items){
		double totalAmount = 0;
		for (BillItem item : items) {
			totalAmount += item.getItemPrice() * item.getQuantity();
		}
		return totalAmount;
	}
	
	/**
	 * Calculates a bill total amount taking into consideration the discount percentage
	 * Discount does not apply to GROCERY
	 * @param items
	 * @param discount
	 * @return double
	 */
	public double calculateDiscountedTotalAmount(List<BillItem> items, int discount){
		
		double totalGroceryAmount = 0;
		double totalDiscountedAmount = 0;
		double itemTotalPrice;
		
		for (BillItem item : items) {
			
			itemTotalPrice = item.getItemPrice() * item.getQuantity();
			if(item.getItemType() != ItemType.GROCERY) {				
				totalDiscountedAmount += itemTotalPrice;
			} else {
				totalGroceryAmount += itemTotalPrice;
			}
		}

		totalDiscountedAmount = this.applyDiscount(totalDiscountedAmount, discount);
		
		return (totalDiscountedAmount + totalGroceryAmount);
	}
	
	/**
	 * Apply a discount percentage for each 100 of a total amount
	 * @param totalAmount
	 * @param discount
	 * @return double
	 */
	public double applyDiscount(double totalAmount, int discount) {
		
		return totalAmount - ( Math.floor(totalAmount/100) * discount );
	
	}
}
