package com.retail.billingservices;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

import com.retail.billingservices.model.Bill;
import com.retail.billingservices.model.BillItem;
import com.retail.billingservices.model.Customer;
import com.retail.billingservices.model.Item;
import com.retail.billingservices.model.ItemType;
import com.retail.billingservices.service.BillingService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BillingServicesApplicationTests {
	
	private BillingService billingService;
	private LocalDate joinDate;
	private HashMap<String, Item> items;
	
	@Before
	public void initTestingValues() {
		this.billingService = new BillingService();
		this.joinDate = LocalDate.now();
		
		this.items = new HashMap<String, Item>();
		this.items.put("MILK", new Item("item-ref-1", "milk", ItemType.GROCERY, 13.2));
		this.items.put("BREAD", new Item("item-ref-2", "bread", ItemType.GROCERY, 4));
		this.items.put("PANT", new Item("item-ref-3", "pants", ItemType.CLOTHES, 16.32));
		this.items.put("SHIRT", new Item("item-ref-4", "shirt", ItemType.CLOTHES, 9.99));
		this.items.put("PANADOL", new Item("item-ref-5", "panadol", ItemType.PHARMACY, 6.43));
		this.items.put("BLENDER", new Item("item-ref-6", "blender", ItemType.ELECTRONICS, 95.99));
	}

	@Test
	public void testEmployeeBill() {

		Customer employee = new Customer("Sam", this.joinDate, true, false);
		
		List<BillItem> itemsList = Arrays.asList(
				new BillItem(this.items.get("MILK"), 4),
				new BillItem(this.items.get("PANT"), 2),
				new BillItem(this.items.get("SHIRT"), 2),
				new BillItem(this.items.get("PANADOL"), 1),
				new BillItem(this.items.get("BLENDER"), 1));
		
		double expectedAmount = 177.84;
		
		Bill customerBill = new Bill();
		customerBill.setCustomer(employee);
		customerBill.setItems(itemsList);
		
		assertThat(this.billingService.calculateBillFinalAmount(customerBill)).isEqualTo(expectedAmount);
	}
	
	@Test
	public void testAffiliateBill() {

		Customer affiliate = new Customer("David", this.joinDate, false, true);
		
		List<BillItem> itemsList = Arrays.asList(
				new BillItem(this.items.get("MILK"), 4),
				new BillItem(this.items.get("PANT"), 2),
				new BillItem(this.items.get("SHIRT"), 2),
				new BillItem(this.items.get("PANADOL"), 1),
				new BillItem(this.items.get("BLENDER"), 1));
		
		double expectedAmount = 197.84;
		
		Bill customerBill = new Bill();
		customerBill.setCustomer(affiliate);
		customerBill.setItems(itemsList);
		
		assertThat(this.billingService.calculateBillFinalAmount(customerBill)).isEqualTo(expectedAmount);
	}
	
	@Test
	public void testOldCustomerBill() {
		
		Customer customer = new Customer("Daisy", this.joinDate.minusYears(3), false, false);
		
		List<BillItem> itemsList = Arrays.asList(
				new BillItem(this.items.get("MILK"), 4),
				new BillItem(this.items.get("PANT"), 2),
				new BillItem(this.items.get("SHIRT"), 2),
				new BillItem(this.items.get("PANADOL"), 1),
				new BillItem(this.items.get("BLENDER"), 1));
		
		double expectedAmount = 202.84;
		
		Bill customerBill = new Bill();
		customerBill.setCustomer(customer);
		customerBill.setItems(itemsList);
		
		assertThat(this.billingService.calculateBillFinalAmount(customerBill)).isEqualTo(expectedAmount);
	}
	
	@Test
	public void testNewCustomerBill() {
		
		Customer customer = new Customer("Sara", this.joinDate.minusYears(1), false, false);
		
		List<BillItem> itemsList = Arrays.asList(
				new BillItem(this.items.get("MILK"), 4),
				new BillItem(this.items.get("PANT"), 2),
				new BillItem(this.items.get("SHIRT"), 2),
				new BillItem(this.items.get("PANADOL"), 1),
				new BillItem(this.items.get("BLENDER"), 1));
		
		double expectedAmount = 207.84;
		
		Bill customerBill = new Bill();
		customerBill.setCustomer(customer);
		customerBill.setItems(itemsList);
		
		assertThat(this.billingService.calculateBillFinalAmount(customerBill)).isEqualTo(expectedAmount);
	}
	
	@Test
	public void testAffiliateOldCustomerBill() {
		
		Customer customer = new Customer("Sara", this.joinDate.minusYears(3), false, true);
		
		List<BillItem> itemsList = Arrays.asList(
				new BillItem(this.items.get("MILK"), 4),
				new BillItem(this.items.get("PANT"), 2),
				new BillItem(this.items.get("SHIRT"), 2),
				new BillItem(this.items.get("PANADOL"), 1),
				new BillItem(this.items.get("BLENDER"), 1));
		
		double expectedAmount = 197.84;
		
		Bill customerBill = new Bill();
		customerBill.setCustomer(customer);
		customerBill.setItems(itemsList);
		
		assertThat(this.billingService.calculateBillFinalAmount(customerBill)).isEqualTo(expectedAmount);
	}

}

