# Retail Billing discount #

Spring Boot demo app for a retail billing discount service

### Maven build ###

To build the project with Maven execute in the poject root directory
```
mvn clean install
```

### Unit Test ###

To run the unit test with Maven execute
```
mvn test
```

### UML Diagram ###

![class_diagram](class_diagram.png)